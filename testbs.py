# -*- encoding: utf-8 -*-
#File : testbs.py
#Time : 2020/3/29 9:46

# BeautifulSoup4将复杂的HTML文档转换成一个复杂的树形结构，每个节点都是python对象，所有对象可以归纳为4种
#
#    - Tag
#    - NavigableString
#    - BeautifulSoup
#    - Comment


from bs4 import BeautifulSoup

file = open("./bs4.html","rb")
html = file.read().decode("utf-8")
bs = BeautifulSoup(html,"html.parser")

#1.Tag 抱歉及其内容，仅可拿到它所找到的第一个内容
# print(bs.title)
# print(bs.a)
# print(bs.head)
# print(type(bs.head))

#2.仅拿到标签里的内容，没有标签（字符串）
# print(bs.title.string)
# print(type(bs.title.string))


#利用attrs可拿到标签里所有的内容，返回的是一个字典
# print(bs.a.attrs)

#3.类型为BeatifulSoup，表示整个文档
# print(type(bs))
# print(bs.name)
# print(bs)

#4.Comment 是一个特殊的NavigableString，输出的内容不包含注释符号
# print(bs.a.string)
# print(type(bs.a.string))
#

#------------------------------------------------实际应用------------------------------------------------


#文档的遍历

# print(bs.head.contents)  #以列表的形式返回这个head标签所有的子内容
# print(bs.head.contents[1])


#文档的搜索

#(1)find_all()
#字符串过滤：会查找与字符串完全匹配的内容
# t_list = bs.find_all("a")
# print(t_list)


import re
#正则表达式搜索：使用search（）方法来匹配内容,只要包含字符串的内容的都会打印出来
# t_list = bs.find_all(re.compile("a"))
# print(t_list)


#方法：传入一个函数（方法）,根据函数的要求来搜索
#返回有内容里name这个字符串的所有标签及其内容
# def name_is_exist(tag):
#     return tag.has_attr("name")
#
# t_list = bs.find_all(name_is_exist)
# for items in t_list:
#     print(items)
#

#2.kwargs   参数

#t_list = bs.find_all(id="head")
#t_list = bs.find_all(class_ = True)
# t_list = bs.find_all(href="http://news.baidu.com")
#
# for items in t_list:
#     print(items)



#3.text参数
#t_list = bs.find_all(text = "hao123")
#t_list = bs.find_all(text = ["hao123","地图","lcm"])
#利用正则表达式
# t_list = bs.find_all(text = re.compile("\d"))  #查找包含特定文本的内容（标签里的字符串）
#
# for items in t_list:
#     print(items)


#4.limit  参数

#t_list = bs.find_all("a",limit=3)

# for items in t_list:
#     print(items)


#css选择器

# t_list = bs.select("title")             #通过标签来查找
# t_list = bs.select(".mnav")             #通过类名来查找
# t_list = bs.select("#wrapper")          #通过id来查找
#t_list = bs.select("a[class='mnav']")    #通过属性来查找
t_list = bs.select("body>div>div>div>div")    #通过子标签来查找
# t_list = bs.select(".mnav ~ .bri")      #通过兄弟标签来查找
#
# print(t_list[0].get_text())

for items in t_list:
    print(items)
