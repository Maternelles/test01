# -*- encoding: utf-8 -*-
#File : textrequests.py
#Time : 2020/5/19 21:55

import requests

#————————————————使用get方法---------------------------------------------

#1.获取网页源代码
response = requests.get("https://www.zhipin.com/job_detail/3780b51f110df7e10Hx92d26FFs~.html?ka=rcmd_list_job_0")

#2.查看响应内容（基本使用）
print(response.text)        #返回的是Unicode格式的数据，经过解码,通过自己的猜测进行解码，所以有时会错误会乱码，当乱码的时候就采用解码后的content方法
# print(response.content.decode("utf-8"))        #返回的字节流数据，content未经过解码,直接从网站上抓取的数据，所以是一个bytes类型。其实在硬盘上和网络上传输的字符串都是bytes类型
# print(response.encoding)        #返回响应头部字符编码
# print(response.status_code)        #返回响应码
# print(response.url)          #返回完整的url地址

# params = {'wd':'中国'}
# headers = {"User-Agent":"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36"}
# response = requests.get("http://www.baidu.com/s",params=params,headers=headers)
#
# print(response.url)


#===================================使用post方法============================================
#
# data = {}
#
# headers ={}
#
# response = requests.post("http://www.baidu.com/",data = data,headers = headers)