from git.repo import Repo
from git.remote import Remote
import os


local_path = 'D://lcm//pythonlcm//clone_code'

# 拉取代码
# Repo.clone_from('https://gitee.com/maternelles/text.git', to_path=download_path, branch='master')
# repo = Repo(local_path)
# repo.git.pull()


# def push_to_git(py_file_path, annotation):
#     """
#     将代码上传至远程仓库
#     :param py_file_path: 要上传至远程仓库的python文件路径
#     :param annotation: 上传python文件时要写的一些注释
#     :return:
#     """
#     # cmdline.execute(('git add ' + py_file_path).split())
#     # cmdline.execute(('git commit -m ' + annotation).split())
#     # cmdline.execute('git push origin master'.split())
#
#     os.system("git add {}".format(py_file_path))
#     os.system("git commit -m {}".format(annotation))
#     os.system("git push origin master")
#
#
def clone_from():
    """
    从远处仓库下载程序到本地
    :return:
    """
    remote_url = 'https://gitlab.com/Maternelles/test01.git'
    local_path = 'D://lcm//pythonlcm//gitlab_clone'
    symbol = os.path.exists(local_path)
    if not symbol:
        Repo.clone_from(remote_url, to_path=local_path, branch='master')
    repo = Repo(local_path)
    # remote = Remote(repo, 'lcm_origin')
    repo.git.pull()


# if __name__ == '__main__':
#     # annotation = '测试3'
#     # py_file_path = 'D://lcm//pythonlcm//text//js'
#     # push_to_git(py_file_path, annotation)
#     clone_from()

import time
# from tools.sendMessageToDD import sendMessage
import subprocess


def add():
    global local_path
    cmd = "git add {}".format(local_path)
    process = subprocess.Popen(cmd, shell=True)
    process.wait()
    returnCode = process.returncode

    if returnCode != 0:
        print(" add returnCode", returnCode)
    else:
        commit()


def commit():
    global commitMessage
    commitMessage = time.strftime("%Y/%m/%d %H:%M")
    cmd = "git commit -m  '{}'".format(commitMessage)

    # print("cmd = " + cmd)
    process = subprocess.Popen(cmd, shell=True)
    process.wait()
    push()


def push():
    cmd = "git push origin master"
    process = subprocess.Popen(cmd, shell=True)
    process.wait()
    returnCode = process.returncode
    if returnCode != 0:
        print("push returnCode", returnCode)
    else:
        """通过websocket进行通信促使各取数机器去更新代码"""
        pass


if __name__ == '__main__':
    clone_from()

    # local_path = 'D://lcm//pythonlcm//text//testxpath.py'
    # commitMessage = "testxpath.py"
    # add()
    # commit()
