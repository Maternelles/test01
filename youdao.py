# -*- encoding: utf-8 -*-
#File : youdao.py
#Time : 2020/5/22 15:21

import hashlib
import requests
import json
import random
import time

def md5(value):
    #创建MD5对象
    md5_obj = hashlib.md5()

    #加密字符串
    md5_obj.update(bytes(value,encoding='utf-8'))

    #进行16位的加密
    sign = md5_obj.hexdigest()
    return sign

def youdao(i):
    #定义初始url
    base_url = "http://fanyi.youdao.com/translate?smartresult=dict&smartresult=rule"
    #获取ts值
    ts = str(int(time.time()*1000))
    #获取salt值
    salt = str(int(time.time()*1000)) + str(random.randint(0,9))
    #获取sign值
    sign = "fanyideskweb" + i + salt + "Nw(nmmbP%A-r6U3EUn]Aj"
    sign = md5(sign)

    data = {
        'i':i,
        'from':'AUTO',
        'to':'AUTO',
        'smartresult':'dict',
        'client':'fanyideskweb',
        'salt':'15901286935302',
        'sign':'2086fd6047c600cc9f0e1c2c2c17b085',
        'ts':'1590128693530',
        'bv':'d50cccdf80e72b2ce6d88dd76d3f5cd6',
        'doctype':'json',
        'version':'2.1',
        'keyfrom':'fanyi.web',
        'action':'FY_BY_CLICKBUTTION'
    }

    headers={
        'Accept':'application/json, text/javascript, */*; q=0.01',
        'Accept-Encoding':'gzip, deflate',
        'Accept-Language':'zh-CN,zh;q=0.9',
        'Connection':'keep-alive',
        'Content-Length':'246',
        'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8',
        'Cookie':'OUTFOX_SEARCH_USER_ID=-954605821@10.169.0.82; OUTFOX_SEARCH_USER_ID_NCOO=60052957.36204125; _ntes_nnid=ca585e502f15b6beb3b4fabed2136470,1589446319567; DICT_UGC=be3af0da19b5c5e6aa4e17bd8d90b28a|; JSESSIONID=abcFF2hGcjSze-g-fc7ix; ___rl__test__cookies=1590128693526',
        'Host':'fanyi.youdao.com',
        'Origin':'http://fanyi.youdao.com',
        'Referer':'http://fanyi.youdao.com/',
        'User-Agent':'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36',
        'X-Requested-With':'XMLHttpRequest'
    }

    response = requests.post(url=base_url,data=data,headers=headers)
    result_data = response.content.decode("utf-8")
    result_data = json.loads(result_data)
    print("翻译的结果是：%s"%result_data['translateResult'][0][0]['tgt'])

if __name__ =='__main__':
    i = input("请输入你想翻译的词：")
    youdao(i)