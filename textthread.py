# -*- encoding: utf-8 -*-
#File : textthread.py
#Time : 2020/6/17 22:35
#在进行开发时，避免不了会遇到要处理并发的情况
# 一般并发的手段有采用多进程和多线程
# 但线程比进程更轻量化，系统开销更低

# 在多线程threading这个库中最核心的内容就是Thread这个类


import threading
import time



# ————————————————————创建Thread对象有两种方法：
#     1.直接创建Thread，将一个callba对象从类的构造器传递进去，这个callbac就是回调函数，用来处理任务
            #class threading.Thread(group=None,target=None,name=None,args=(),kwargs={},*,daemon=None)
            #在此方法中，target参数是最重要的，我们需要将一个callba函数赋值给它，线程才能正常运行
            #而要让一个Thread对象启动，调用它的start（）方法即可
# def test():
#     for i in range(5):
#         print('test',i)
#         time.sleep(1)
#
# thread = Thread(target = test)
# thread.start()
#
# for i in range(5):
#     print('main',i)
#     time.sleep(1)
            #每一个Thread都有一个name的属性，代表的就是线程的名字，这个可以在构造方法中赋值
            #如果在构造方法中没有那么赋值的话，默认就是“Thread-N”的形式，N是数字
# def test():
#     for i in range(5):
#         print(threading.current_thread().name+'test',i)
#         time.sleep(2)
# #在Thread（）中有一个daemin参数，默认是None，在默认值下若主线程运行完会等待子线程结束，其主线程自身才结束
# #若要达到主线程结束，子线程也立刻结束，则可将daemon值设为True
# thread = threading.Thread(target = test,name='TestThread',daemon=True)
# thread.start()
# #通过join（）方法提供线程阻塞手段
# #本来此两个线程是同时运行的，join（）方法可以让一个先运行，一个后运行
# #通常情况先join（）会一直等待对应线程的结束，但可以通过参数赋值，等待规定的时间就好了
# #join（timeout=None）
# thread.join(1.0)#这样使得主线程只等待了一秒
#
# for i in range(5):
#     print(threading.current_thread().name+'main',i)
#     print(thread.name+'is alive',thread.isAlive())#isAlive（）方法可以查看此线程是否还在运行
#     time.sleep(1)


#——————————————————————————————————————————————————————————————
#     2.编写一个自定义类继承Thre，然后复写run（）方法，在run（）方法中编写任务处理代码，然后创建这个Thread的子类。
class TestThread(threading.Thread):

    def __init__(self,name=None):
        threading.Thread.__init__(self,name=name)


    def run(self):
        for i in range(5):
            print(threading.current_thread().name+'test',i)
            time.sleep(1)


thread = threading.Thread(name='TestThread')
thread.start()


for i in range(5):
    print(threading.current_thread().name+'main',i)
    print(thread.name+'is alive',thread.isAlive())
    time.sleep(1)


