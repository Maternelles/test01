# -*- encoding: utf-8 -*-
#File : tesyrRE.py
#Time : 2020/3/29 17:08


#正则表达式：字符串模式（判断字符串是否符合一定的标准

import re

pat = re.compile("AA")        #此处的字符串是正则表达式，用来验证其他的字符串是否与此匹配
# m = pat.search("AAC")             #search字符串是被校验的内容
# m = pat.search("Caa")
m = pat.search("sdgrrrrrCAAfreegAA")
print(m)

#没有模式对象
# m = re.search("bnm","xccvbnm")     #前面的字符串为规则（模板），后面的字符串为校验的对象
# print(m)


#print(re.findall("a","ADSDFEaddffa"))  #前面的字符串为规则（模板），后面的字符串为校验的对象
# print(re.findall("[A-Z]","ADSDFEaDDffa"))    #将所有大写字母打印出来
# print(re.findall("[A-Z]+","ADSDFEaDDffa"))



#sub  替换

#print(re.sub("a","A","dsacfgeafrea"))       #找到a用A替换

#建议在正则表达式中，被比较的字符串前面加上r，不用担心转义字符的问题
a = r"\aabd-\'"
b = "\aabd-\'"

print("lcm")
print("lcm")
print("lcm")