from git.repo import Repo
import os


def clone_from():
    """
    从远处仓库自动下载程序到本地
    :return:
    """
    remote_url = 'https://gitlab.com/Maternelles/test01.git'   #远程仓库地址
    local_path = 'D://lcm//pythonlcm//gitlab_clone'    #本地存储远程仓库代码的路径
    symbol = os.path.exists(local_path)
    if not symbol:
        Repo.clone_from(remote_url, to_path=local_path, branch='master')
    repo = Repo(local_path)
    repo.git.pull()


if __name__ == '__main__':
    clone_from()

