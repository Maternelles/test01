# -*- encoding: utf-8 -*-
#File : textXlwt.py
#Time : 2020/3/30 17:12


#基本思路
"""
 以utf-8编码创建一个Excel对象
 创建一个Sheet表
 往单元格写入内容
 保存表格
"""

import xlwt


workbook = xlwt.Workbook(encoding="utf-8")
worksheek = workbook.add_sheet("sheetlcm")
for i in range (0,9):
    for j in range (0,i+1):
        a = i+1
        b = j+1
        c = a*b
        worksheek.write(i,j,"%d * %d = %d"%(a,b,c))

workbook.save("lyb.xls")